// Who		: Mike Coles
// What		: API example
// When		: 20180723@00501z
// Why		: Demonstrate usage of golang
//             Demonstrate API calls
//             Demonstrate logging to Graylog2
//             Prep work for chatbot calls
// How		: By listening to FFDP
// Caveats	: Limited scoping and some simplification to keep code digestible
//             json encoder is (ab)used for all responses for simplicity
// 	 		   IP lookup only handles RIPE entries
// Features	: ip - ip subnet calculator. Maybe renamed to ipcalc
//             whois - Lookup domain names
//             maclookup - Lookup Organizationally Unique Identifier (OUI) of mac addresses
//             ldaplookup - look up user by CN, IP(?), and ???
//			   What else?
// TODO		: Prometheus - Demonstrate API metrics with Prometheus
//			  Add API versioning http://www.aaron-torres.com/post/api-versioning-gorilla-mux/
// Ripped	: <Include references to ripped code>
              
// This is a standalone app so we are `main`
package main



// Import external libraries
import (
	// Handle the icky parts of JSON encoding
    "encoding/json"
	// mux provides the mapping of URLs to endpoints and variable extraction
    "github.com/gorilla/mux"
	// log provides the loggin functionality
	"log"
	// net/http provides the web service foundation
    "net/http"
//    "net/url"
	// Handle whois lookups
    "github.com/likexian/whois-go"
    "github.com/likexian/whois-parser-go"
	// flag allows retrieval of arguments
    "flag"
	// go-gelf provides support for outbound gelf formatted logs
    "github.com/Graylog2/go-gelf/gelf"
	"io"
	"io/ioutil"
	"os"
	// Handle IP lookups, FIXME: need a full solutions
    "github.com/digineo/ripego"
//    "fmt"
	"strconv"
//    "strings"
)

var ZulipWebHook string
var ZulipBotName string
var ZulipBotKey string
var ZulipStream string

// Provide a list of API endpoints
func HomeHandler (w http.ResponseWriter, r *http.Request) {
  json.NewEncoder(w).Encode("Available endpoints /whois /ldaplookup /maclookup /ip")
}

// function to lookup address space owners
// BORKED!! - Only handles RIPE entries.
// re-evaluating procedure
func IPLookup (w http.ResponseWriter, r *http.Request) {
    params := mux.Vars(r)
    ip, err := ripego.IPLookup(params["query"])

	if err != nil {
		log.Println(err)
	}

	json.NewEncoder(w).Encode("Inetnum: " + ip.Inetnum)
	json.NewEncoder(w).Encode("Desc: " + ip.Descr)
}

// Perform a `whois` lookup for IP? Domain?
func WhoisLookup (w http.ResponseWriter, r *http.Request) {
	// Extract values from HTTP
    params := mux.Vars(r)
	
    if (params["query"] != "") {
        //json.NewEncoder(w).Encode("Whois Called with param "+params["query"])
        log.Printf("Whois Called with param "+params["query"])
        //result, err := whois_parser.Parse(params["query"])
        result, err := whois.Whois(params["query"])
        
        //json.NewEncoder(w).Encode(err)
        if err == nil {
            parsed, err := whois_parser.Parse(result)
            if (params["format"] == "json") {
                json.NewEncoder(w).Encode(parsed)
                
            } else {
            
            // Print the domain status
            if err == nil {
                // Test zulip MD
                // fmt.Printf(w,"%s","\n\n\n")
                json.NewEncoder(w).Encode(
                    // Print the domain created date
                    parsed.Registrar.CreatedDate+"  \n"+
                    // Print the domain expiration date
                    parsed.Registrar.ExpirationDate+"  \n"+
                    // Print the registrant name
                    parsed.Registrant.Name+"  \n"+
                    // Print the registrant email address
                    parsed.Registrant.Email+"  \n"+
                    parsed.Registrant.City+"  \n"+
                    parsed.Registrant.Province+"  \n"+
                    parsed.Registrant.Country)
            }
            }
        } 
    } else {
        json.NewEncoder(w).Encode("Whois called with no parameters")
        log.Printf("Whois called with no parameters")
    }   
}

// Placeholder for LDAP lookup
func LDAPLookup (w http.ResponseWriter, r *http.Request) {
    json.NewEncoder(w).Encode("LDAP Lookup Called")
}

func MACLookup (w http.ResponseWriter, r *http.Request) {
// Perform a lookup for a MAC address Organizationally Unique Identifier (OUI)
    params := mux.Vars(r)
    if (params["query"] != "") {
        json.NewEncoder(w).Encode("maclookup Called with param "+params["query"])
		resp, err := http.Get("http://macvendors.co/api/"+params["query"]+"/pipe")
		if err != nil {
		  json.NewEncoder(w).Encode("Whoops after call to api.macvendors.com")
		  log.Fatalf("gelf.NewWriter: %s", err)
		} else {
		  defer resp.Body.Close()
		  body, _ := ioutil.ReadAll(resp.Body)
		  json.NewEncoder(w).Encode("Got Response: "+string(body))
		}
	} else {
        json.NewEncoder(w).Encode("maclookup called with no parameters")
    }    
}

func LB_Status (w http.ResponseWriter, r *http.Request) {
// Dummy function to return 100% readiness
  log.Printf("LB status called. Giving BS answer of 100")
  json.NewEncoder(w).Encode(100)
}

func SIEM_Alert_Add (w http.ResponseWriter, r *http.Request) {
// Inject SIEM alert into Zulip to be picked up by lambot
// local vars

        
    params := mux.Vars(r)
      if (params["IP"] != "") {
        json.NewEncoder(w).Encode("SIEM_Alert_Add Called with param "+params["IP"])

// Tried to post the variables, screw it, get them instead.
//        v := url.Values{}
//        v.Set("type", "stream")
//        v.Add("to", ZulipStream)
//        v.Add("subject", params["IP"])
//        v.Add("content", "lambot update siem_alert_add with IP:"+params["IP"])
//        s := v.Encode()
//        fmt.Printf("v.Encode(): %v\n", s)
//        req, err := http.NewRequest("POST", ZulipWebHook+"?type=stream&content=lambot update siem_alert_add with IP:"+params["IP"]+"&to="+ZulipStream+"&subject="+params["IP"], strings.NewReader(v.Encode()))

        req, err := http.NewRequest("POST", ZulipWebHook+"?type=stream&content=lambot update siem_alert_add with IP:"+params["IP"]+"&to="+ZulipStream+"&subject="+params["IP"], nil )
        req.SetBasicAuth(ZulipBotName, ZulipBotKey)
        cli := &http.Client{}
        resp, err := cli.Do(req)      
		if err != nil {
		  json.NewEncoder(w).Encode("Whoops after attempt to inject message in Zulip")
		  log.Fatalf("gelf.NewWriter: %s", err)
		} else {
		  defer resp.Body.Close()
		  body, _ := ioutil.ReadAll(resp.Body)
		  json.NewEncoder(w).Encode("Got Response: "+string(body))
		}
	} else {
        json.NewEncoder(w).Encode("SIEM_Alert_Add called with no parameters")
    }  
}
  
// main is the default golang procedure that is run 
func main() {
	// Associate the graylog server from arguments if set
	var graylogAddr string
	flag.StringVar(&graylogAddr, "graylog", "", "Graylog server addr - [graylog.domain]")
	// Associate the listenport from arguments, if set
	var listenPort int 
	flag.IntVar(&listenPort, "listenport", 8000, "port for rest API to listen on")
	// Define LDAP LDAP host 
	var ldapHost string
	flag.StringVar(&ldapHost, "ldaphost", "", "LDAP host - [ldap.domain]")
	// Define LDAP Port
	var ldapPort int 
	flag.IntVar(&ldapPort,"ldapport", 389, "port for LDAP server")
	// Define LDAP Methods
	var ldapMethod string
	flag.StringVar(&ldapMethod, "ldapmethod", "ldap://", "ldap:// or ldaps://")
    // Zulip Webook URL
        flag.StringVar(&ZulipWebHook, "zulipwebhook", "", "https://zulip.some.domain")
    // ZulipBotName
        flag.StringVar(&ZulipBotName, "zulipbotname", "", "some-bot@zulip.some.domain")
    // ZulipBotKey
        flag.StringVar(&ZulipBotKey, "zulipbotkey", "", "lkgjfkgjkfsgjsjgfl")
    // Zulip Stream
        flag.StringVar(&ZulipStream, "zulipstream", "", "Stream-Name")
    
	// Extract the arguments
	flag.Parse()
	
	//If graylog argument if set, attempt to log to the graylog server
	if graylogAddr != "" {
		gelfWriter, err := gelf.NewWriter(graylogAddr)
		if err != nil {
			log.Fatalf("gelf.NewWriter: %s", err)
		}
		// log to both stderr and graylog2
		log.SetOutput(io.MultiWriter(os.Stderr, gelfWriter))
		log.Printf("logging to stderr & graylog2@'%s'", graylogAddr)
	}
	
    
    // start a new mux router to handle HTTP requests
    router := mux.NewRouter()

    router.HandleFunc("/v1/whois/{query}/{format}", WhoisLookup).Methods("GET")
    router.HandleFunc("/v1/whois/{query}", WhoisLookup).Methods("GET")
    router.HandleFunc("/v1/whois", WhoisLookup).Methods("GET")
    router.HandleFunc("/v1/ip/{query}/{format}", IPLookup).Methods("GET")
    router.HandleFunc("/v1/ip/{query}", IPLookup).Methods("GET")
    router.HandleFunc("/v1/ip", IPLookup).Methods("GET")
    router.HandleFunc("/v1/ldaplookup/{query}", LDAPLookup).Methods("GET")
    router.HandleFunc("/v1/ldaplookup", LDAPLookup).Methods("GET")
    router.HandleFunc("/v1/maclookup", MACLookup).Methods("GET")
    router.HandleFunc("/v1/maclookup/{query}", MACLookup).Methods("GET")
	router.HandleFunc("/v1/lb/", LB_Status).Methods("GET")
    router.HandleFunc("/v1/siem_alert_add/{IP}", SIEM_Alert_Add).Methods("GET")
	router.HandleFunc("/v1/", HomeHandler).Methods("GET")
	router.HandleFunc("/", HomeHandler).Methods("GET")
	// start the service, log any fatal errors
    log.Fatal(http.ListenAndServe(":"+strconv.Itoa(listenPort), router))
}
