//Who       : Mike Coles

//What      : API example

//When      : 20180723@00501z

//Why       : Demonstrate usage of golang

//            Demonstrate API calls

//            Demonstrate logging to Graylog2

//            Prep work for chatbot calls

//How       : By listening to FFDP

//Caveats   : Limited scoping and some simplification to keep code digestible

//            json encoder is (ab)used for all responses for simplicity

//			  IP lookup only handles RIPE entries

//Features  : ip - ip subnet calculator. Maybe renamed to ipcalc

//            whois - Lookup domain names

//            maclookup - Lookup Organizationally Unique Identifier (OUI) of mac addresses

//            ldaplookup - look up user by CN, IP(?), and ???

//			  What else?

//ToDo 		: Prometheus - Demonstrate API metrics with Prometheus

//Ripped	: <Include references to ripped code>


Once you get golang's silly directory structure figured out, just run `go build` and run `curl http://127.0.0.1/`

Pull and PRs welcomed.
